
import { useState, useEffect } from "react";
import { CategorieService } from "../shared/categories-service";


const intialState = {

    description: '',
    date: '',
    montant: '',
    categorieId: '',
    categorie: ''

}


export function FormOps({ onFormSubmit }) {

    const [form, setForm] = useState(intialState);

    const [cats, setCats] = useState([]);



    async function fetchCats() {
        const response = await CategorieService.findAllCAt();
        setCats(response.data);
    }




    const handleChange = event => {

        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }


    const handleSubmit = event => {
        event.preventDefault();
        onFormSubmit(form);



    }


    useEffect(() => {
        fetchCats();
    }, []);

    return (

        <form onSubmit={handleSubmit}>

            <label>Description :</label>
            <input className="form-control" required type="text" name="description" onChange={handleChange} />
            <label>Date :</label>
            <input className="form-control" required type="date" name="date" onChange={handleChange} />
            <label>Montant :</label>
            <input className="form-control" type="number" name="montant" onChange={handleChange} />



            <label>Catégorie :</label>


            <select className="form-control" name="categorieId" onChange={handleChange}>
            <option value="">--Choix catégorie--</option>

                {cats.map(item =>
                    <option key={item.id}
                        value={item.id}
                    >{item.nom}</option>
                )}
            
            </select>


            {/* <input className="form-control" type="number" min="0" max="10"name="categorieId" onChange={handleChange} />  */}



            <button className="btn btn-primary">Ajouter</button>

        </form>

    );
}
