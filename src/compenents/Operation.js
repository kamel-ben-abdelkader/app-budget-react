
export function Operation({ operation, onDelete }) {
   
   

    return (
        <div className="col-12">
            <article className="card">
                <div className="card-body">
                    <h5 className="card-title">Date : {new Intl.DateTimeFormat('fr-FR').format(new Date(operation.date))}</h5>
                    <p className="card-text"> Description : {operation.description}</p>
                    <p className={operation.montant < 0 ? 'card-text minus' : ' card-text plus'}> Montant : {Number.parseFloat(operation.montant).toFixed(2)} €</p>
                    <p className="card-text"> Categorie :   {operation.categorie.nom}</p>

                </div>

                <div className="card-footer text-end">
                    <button className="btn btn-primary" onClick={() => onDelete(operation.id)}>Supprimez</button>
                </div>

            </article>
        </div>
    );
}