import "./Graphique.css"
import { Bar, Line, Doughnut, Pie } from "react-chartjs-2"

// import { useState} from "react";
// import { CategorieService } from "../../shared/categories-service";
// import { OperationService } from "../../shared/operations-service";



  //   async function fetchCats(id) {
  //     const response = await  OperationService.findCatKey(id);
  //     setOps(response.data);
  // }
  // // test recherche par categorie



export const GraphiqueBar = () => {


    const data = {

        labels: ["Sortie", "Loisirs", "Loyer", "Salaires", "Alimentation", "Shopping"],
        
        datasets : [
        
            {
                label : "Graphique des dépenses",
                data :["12","16", "12","32","6", "2"],
                backgroundColor: ["Red", "Blue", "Green", "Violet", "Orange", "Yellow"],
        
            },
        ],
        }
        
        const options= {
            maintainAspectRatio : false,
        }
        


    
    return (
     < div className='Graphique'>
          < div className='containergraph'>
          < div className='cardgraph'>
              <Bar data={data} />
              </div>

              < div className='cardgraph'>
              <Line data={data} options={options} />
              </div>
              < div className='cardgraph'>

              <Doughnut data={data} options={options} />
              </div>

              < div className='cardgraph'>
              <Pie data={data} options={options} />
              </div>




          </div>
          </div>


    )
  }
  

