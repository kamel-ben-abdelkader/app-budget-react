import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import { Grid, InputAdornment, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const StyledTextField = withStyles((theme) => ({
    root: {
        "& .MuiInputBase-root": {
            color: 'white',
            borderColor: 'white'
        },
        "& .MuiFormLabel-root": {
            color: 'white'
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'white',
            }
        }
    }
}))(TextField);



export function SearchBar({ onSearch }) {
    const handleChange = (event) => {
        onSearch(event.target.value);
    }

    return (
        <AppBar position="static">
            <Toolbar>
                <Grid container alignItems="center" justify="space-between">
                    <Grid item>
                        <Typography variant="h6" noWrap>
                           Budget APP
                        </Typography>
                    </Grid>
                    <Grid item>
                        <div >

                            <StyledTextField
                                label="Search..."
                                InputProps={{
                                    'aria-label': 'search',
                                    startAdornment: <InputAdornment position="start"><SearchIcon /></InputAdornment>
                                }}
                                onChange={handleChange}
                                variant="outlined"
                            />

                        </div>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}
