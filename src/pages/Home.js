
import React from "react";
import { useEffect, useState } from "react";
import { FormOps } from "../compenents/FormOperations";
import { Operation } from "../compenents/Operation";
import { OperationService } from "../shared/operations-service";
import { DropDownMonth } from "../compenents/DropDownMonth";
import { Header } from "../compenents/Header";
// import { GraphiqueBar } from "../compenents/Graphique/GraphiqueBar";




export function Home() {

  const [ops, setOps] = useState([]);


  async function fetchOps() {
    const response = await OperationService.findAll();
    setOps(response.data);
  }


  function totalPrice() {
    let total = 0;
    for (const iterator of ops) {

      total += Number(iterator.montant)
    }
    return total

  }


  <h2>Soldes : {totalPrice()} €</h2>

  async function deleteOp(id) {
    await OperationService.delete(id);
    setOps(
      ops.filter(item => item.id !== id)
    );
  }

  async function addOp(operation) {
    const response = await OperationService.add(operation);
    setOps([
      ...ops,
      response.data
    ]);
  }



  async function fetchMonth(month) {
      const reponse = await OperationService.fetchMonth(month);
      setOps(reponse.data);
    }



  useEffect(() => {
    fetchOps();
  }, []);



  return (


    <div>
       <Header />
      {/* <SearchBar onSearch={handleSearch} /> */}

      <section className="row">

        

        <div className="col-6">
          <h2>Ajout operations bancaires</h2>
          <FormOps onFormSubmit={addOp} />

        </div>
        <DropDownMonth reset={fetchOps} selectMonth= {fetchMonth}/>
        <div className="col-6">
          <h2>Operations bancaires</h2>
        
          <h2>Total Price : {totalPrice().toFixed(2)} €</h2>

          {ops.map(item =>
            <Operation key={item.id}
              operation={item}
              onDelete={deleteOp} />
          )}
        </div>

        {/* <GraphiqueBar /> */}

      </section>


      {/*      
          <button onClick={fetchOps}>Voir mes Opérations</button>

          {ops.map(item => <p key={item.id}>Operation n°{item.id} : Description : {item.description}, Date : {item.date} , Montant : {item.montant},
          catégorie : {item.categorieId}</p>)} */}

    </div>
  )
}
