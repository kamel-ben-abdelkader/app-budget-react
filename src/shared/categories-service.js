import axios from "axios";

export class CategorieService {
    
    static async findAllCAt() {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/categories');
    }

    static async delete(id) {
        axios.delete(process.env.REACT_APP_SERVER_URL+'/api/budget/categories' + id);
    }

    static async add(operation) {
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/budget/categories',operation);
    }

//     static async fetchOne(id) {
//         return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/operations/' + id);
//     }
}