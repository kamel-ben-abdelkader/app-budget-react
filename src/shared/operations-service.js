import axios from "axios";

export class OperationService {
    
    static async findAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/operations');
    }

    static async search(term) {

        const response = await axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/operations?search='+term);
        return response.data;
    }


    static async findKey() {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/operations/test');
    }

    static async findCatKey(id) {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/operations/categories/' + id);
    }

    static async fetchMonth(month) {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/operations/month/' + month);
    }



    static async delete(id) {
        axios.delete(process.env.REACT_APP_SERVER_URL+'/api/budget/operations/' + id);
    }

    static async add(operation) {
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/budget/operations',operation);
    }

//     static async fetchOne(id) {
//         return axios.get(process.env.REACT_APP_SERVER_URL+'/api/budget/operations/' + id);
//     }
}