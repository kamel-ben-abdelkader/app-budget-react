
### `Projet : `

<img src="./public/images/usecase.png" width="300" height="300" />


L'objectif de ce projet était de créer une application fullstack pour gérer une application budget.

 avec Node.JS et React.


### `Realisation:`

Premièrement j'ai créé un diagramme de classes avec mes entités :


<img src="./public/images/Class.png" width="300" height="300" />

Ensuite j'ai décidé de créé deux tables plutot qu'une qui correspondent à mes deux entités de mon diagramme dans la BDD grâce à un script sql avec quelques données en insertion. 
J'ai utilisé Node.js avec express pour créer les routes et les repository de chaque entité coté backend.

J'ai réalisé une jointure SQL sur mes 2 tables 

<img src="./public/images/jointuretables.png" width="300" height="300" />


Pour le front j'ai utilisé React , bootstrap , react-chartjs-2 (pour les graphiques) et Axios pour les requêtes AJAX.



### `Techno: `

- Node.js
- React
- Sql


### `Amelioration: `
exemple : 
- Restructuration du style pour une meilleure UX (en utilisant une librarie comme Material UI) 
- Mise en place des graphiques avec la database
- Mise en place d'une Search Bar
- ...




### `Liens: `

- Backend Gitlab : https://gitlab.com/kamel-ben-abdelkader/app-budget
- Backend hébergé sur Heroku : https://app-budget-simplon.herokuapp.com/api/budget/operations
- Site hébergé sur Netlify : https://objective-swanson-17156a.netlify.app/
